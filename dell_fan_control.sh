#!/bin/bash

####################################################################################

IDRACIP="IDRAC_IP"
IDRACUSER="USERNAME"
IDRACPASSWORD="SEEKRIT_PASSWORD"

STATICSPEED="0x0f" # Static fan speed
MAXTEMP=30 # Ambient Temp Threshold
MAXCPUTEMP=55 # Core Temp Threshold

COLOURS=1 # Enable Colours

####################################################################################

if [ $COLOURS -eq "1" ];
  then
        RED='\e[31m'
        GRN='\e[32m'
        NC='\e[0m'
   else
        RED=
        GRN=
        NC=
fi

while :
do
clear

# Check all CPU cores and count the cores that are over threshold into an array
CORES=($(sensors | grep -oP 'Core.*?\+\K[0-9]+'))
core_array=${#CORES[@]}
HOTCORECOUNT=0 # Reset HOTCORECOUNT

echo "Counted $core_array cpu values"

for item in "${CORES[@]}"; do
  if [ $MAXCPUTEMP -gt $item ]; then
    #printf "CPU core temp $item present in the array, below threshold of $MAXCPUTEMP"
    echo -e "CPU core temp ${GRN}$item${NC} present in the array, below threshold of $MAXCPUTEMP"
  else
    #printf "CPU core temp $item present in the array, over threshold of $MAXCPUTEMP"
    echo -e "CPU core temp ${RED}$item${NC} present in the array, over threshold of $MAXCPUTEMP"
    ((HOTCORECOUNT++))
  fi
done

echo "Hot Core Count is :" $HOTCORECOUNT
AMBTEMP=$(ipmitool -I lanplus -H $IDRACIP -U $IDRACUSER -P $IDRACPASSWORD sdr type temperature | grep Ambient | cut -d"|" -f5 | cut -d" " -f2)

if [ $AMBTEMP -lt $MAXTEMP ];
  then
    echo -e "Current Temperature :" ${GRN}${AMBTEMP}${NC} at $(date)
  else
    echo -e "Current Temperature :" ${RED}$AMBTEMP${NC} at $(date)
fi

if [[ $AMBTEMP -lt $MAXTEMP ]] && [[ $HOTCORECOUNT -eq "0" ]]
  then
   # If Ambient temp is below threshold and there are no hot cores, set static fan speed.
    ipmitool -I lanplus -H $IDRACIP -U $IDRACUSER -P $IDRACPASSWORD raw 0x30 0x30 0x01 0x00
    echo "    Dynamic fan control disabled."
    ipmitool -I lanplus -H $IDRACIP -U $IDRACUSER -P $IDRACPASSWORD raw 0x30 0x30 0x02 0xff $STATICSPEED
    echo "    Static fan speed set."
  else
   # If Ambient temp is above threshold or there are hot cores, enable dyanmic fan control.
    ipmitool -I lanplus -H $IDRACIP -U $IDRACUSER -P $IDRACPASSWORD raw 0x30 0x30 0x01 0x01
    echo "      Dynamic fan control enabled."
fi

        sleep 15

done
