# R710_fan_control

Small batch script that monitors CPU and Ambient temps on a Dell PowerEdge R710 Chassis (and similar) and controls fans accordingly.

Features:
 - Set a static (low RPM and quiet) fan speed that the machine will default to if all temps are within threshold.
 - Enables dynamic fans if thresholds are passed.

Requires:
 - ipmitool - "apt-get install ipmitool"
 - sensors - "apt-get install lm-sensors"
 
Settings: 
 - STATICSPEED="0x0f" - is the static fan speed you wish to maintain when cool 
 - MAXTEMP=30 - is the Ambient/Board temperature threshold
 - MAXCPUTEMP=50 - is the CPU temperature threshold

Static Speeds are a decimal value from 1-100 representing percentage in HEX, 0x00 being 0% and 0x64 being 100%.
> 0x00 = 0%, 0x0f = 15%, 0x32 = 50%, 0x64 = 100%


 I currently have it running on my ProxMox host in screen with the below command (not the best idea but it works for now):

> screen -S fancontrol ./dell_fan_control.sh"
